<?php

/**
 * @file
 * Holds admin configuration.
 */

/**
 * Creates the form to manage cron interval actions.
 *
 * @return array
 *   Array containing a form.
 */
function cron_interval_actions_form() {
  // Create new form array.
  $form = array();

  // Allow field grouping.
  $form['#tree'] = TRUE;

  // Attach custom css.
  $form['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'cron_interval_actions') . '/css/cron_interval_actions.css',
    ),
  );

  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron Interval Actions status'),
  );

  $form['status']['cron_interval_actions_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Cron Interval Actions'),
    '#default_value' => variable_get('cron_interval_actions_enabled', FALSE),
    '#description' => t('Allow the intervals and actions to be executed.'),
  );

  // Create the configuration part.
  $form['configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron Interval Actions Configuration'),
    '#description' => t('Make sure the actual cron matches the smallest interval you choose.') . '<p>' . t('<strong>CAUTION:</strong> A test run will execute the code normally triggered by the cron.') . '</p>',
  );

  $intervals = cron_interval_actions_load_intervals();

  if (!empty($intervals)) {
    foreach ($intervals as $interval) {
      cron_interval_actions_fields($form, $interval);
    }
  }

  // Add fields for adding a new interval.
  cron_interval_actions_fields($form);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Helper function to add fields per interval to cron_interval_actions_form.
 */
function cron_interval_actions_fields(&$form, $default_values = array()) {
  // Create array with 31 days.
  $day_options = array();
  for ($i = 1; $i <= 31; $i++) {
    if ($i < 10) {
      $day_options[$i] = '0' . $i;
    }
    else {
      $day_options[$i] = $i;
    }
  }

  // Create array with 24 hours.
  $hours = array();
  for ($i = 0; $i < 24; $i++) {
    if ($i < 10) {
      $hours[$i] = '0' . $i;
    }
    else {
      $hours[$i] = $i;
    }
  }

  // Create array with 60 minutes.
  $minutes = array();
  for ($i = 0; $i < 60; $i++) {
    if ($i < 10) {
      $minutes[$i] = '0' . $i;
    }
    else {
      $minutes[$i] = $i;
    }
  }

  // Add default values.
  $default_values += array(
    'id' => 'new',
    'name' => '',
    'machine_name' => '',
    'interval_data' => '',
    'last_run' => 0,
    'next_run' => 0,
    'status' => 1,
  );

  // Format last run.
  if (empty($default_values['last_run'])) {
    $default_values['last_run'] = t('never');
  }
  else {
    $default_values['last_run'] = format_date($default_values['last_run'], 'custom', 'l j F Y H:i');
  }

  // Format next run.
  if (empty($default_values['next_run'])) {
    $default_values['next_run'] = t('never');
  }
  else {
    $default_values['next_run'] = format_date($default_values['next_run'], 'custom', 'l j F Y H:i');
  }

  // Unserialize interval data.
  $interval_values = (array) unserialize($default_values['interval_data']);

  // Add default interval values.
  $interval_values += array(
    'main_interval' => '',
    'main_interval_interval' => 1,
    'main_interval_interval_minute_snap' => FALSE,
    'sub_interval_month' => '',
    'sub_interval_day' => '',
    'sub_interval_weekday' => '',
    'sub_interval_hour' => '',
    'sub_interval_minute' => '',
    'sub_interval_custom' => '',
    'sub_interval_custom_starting_hour' => '',
    'sub_interval_custom_starting_minute' => '',
  );

  // Create a different fieldset header for a new interval.
  if ($default_values['id'] == 'new') {
    $form['configuration'][$default_values['id']] = array(
      '#type' => 'fieldset',
      '#title' => t('Add new interval'),
      '#description' => t('On adding a new interval, a hook name wil be generated from the interval name. Once created, this hook name can not be changed anymore.'),
      '#attributes' => array(
        'class' => array(
          'cia_interval_wrapper',
        ),
      ),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
  }
  // The fieldset header for an existing interval contains extra data.
  else {
    $form['configuration'][$default_values['id']] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($default_values['name']),
      '#attributes' => array(
        'class' => array(
          'cia_interval_wrapper',
        ),
      ),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['configuration'][$default_values['id']]['last_run_info'] = array(
      '#markup' => '<div>' . t('Last execution time: <strong>@last_run</strong>', array(
        '@last_run' => $default_values['last_run'],
      )) . '</div>',
    );

    $form['configuration'][$default_values['id']]['next_run_info'] = array(
      '#markup' => '<div>' . t('Next execution time: <strong>@next_run</strong>', array(
        '@next_run' => $default_values['next_run'],
      )) . '</div>',
    );

    $hook = cron_interval_actions_generate_hook($default_values['machine_name']);

    $form['configuration'][$default_values['id']]['hook_info'] = array(
      '#markup' => '<div><p>' . t('Available hook: <strong>@hook</strong>', array(
        '@hook' => 'MY_MODULE_' . $hook,
      )) . '</p></div>',
    );
  }

  $form['configuration'][$default_values['id']]['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $default_values['name'],
    '#size' => 20,
  );

  $form['configuration'][$default_values['id']]['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $default_values['machine_name'],
    '#machine_name' => array(
      'exists' => 'cron_interval_actions_machine_name_exists',
      'source' => array('configuration', $default_values['id'], 'name'),
      'label' => t('Machine name'),
    ),
    '#disabled' => ($default_values['id'] == 'new') ? FALSE : TRUE,
    '#size' => 20,
    '#required' => FALSE,
  );

  // Enter an interval number for the main interval.
  $form['configuration'][$default_values['id']]['interval_data']['main_interval_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('#'),
    '#attributes' => array(
      'class' => array('main-interval-interval'),
    ),
    '#default_value' => $interval_values['main_interval_interval'],
    '#states' => array(
      'invisible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'none'),
          array('value' => 'custom'),
        ),
      ),
    ),
  );

  // Select the main interval.
  $form['configuration'][$default_values['id']]['interval_data']['main_interval'] = array(
    '#type' => 'select',
    '#title' => t('Perform actions every'),
    '#options' => array(
      'none' => t('Select an interval'),
      'minute' => t('Minute(s)'),
      'hour' => t('Hour(s)'),
      'day' => t('Day(s)'),
      'week' => t('Week(s)'),
      'month' => t('Month(s)'),
      'year' => t('Year'),
      'custom' => t('Custom'),
    ),
    '#default_value' => $interval_values['main_interval'],
  );

  // Select a month when year is selected as main interval.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_month'] = array(
    '#type' => 'select',
    '#title' => t('Month'),
    '#options' => array(
      1 => t('January'),
      2 => t('February'),
      3 => t('March'),
      4 => t('April'),
      5 => t('May'),
      6 => t('June'),
      7 => t('July'),
      8 => t('August'),
      9 => t('September'),
      10 => t('October'),
      11 => t('November'),
      12 => t('December'),
    ),
    '#default_value' => $interval_values['sub_interval_month'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array('value' => 'year'),
      ),
    ),
  );

  // Select a day if year or month is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_day'] = array(
    '#type' => 'select',
    '#title' => t('Day'),
    '#options' => $day_options,
    '#default_value' => $interval_values['sub_interval_day'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'year'),
          array('value' => 'month'),
        ),
      ),
    ),
  );

  // Select a weekday if week is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_weekday'] = array(
    '#type' => 'select',
    '#title' => t('Day'),
    '#options' => array(
      'Monday'    => t('Monday'),
      'Tuesday'   => t('Tuesday'),
      'Wednesday' => t('Wednesday'),
      'Thursday'  => t('Thursday'),
      'Friday'    => t('Friday'),
      'Saturday'  => t('Saturday'),
      'Sunday'    => t('Sunday'),
    ),
    '#default_value' => $interval_values['sub_interval_weekday'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array('value' => 'week'),
      ),
    ),
  );

  // Select an hour if year, month, week or day is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_hour'] = array(
    '#type' => 'select',
    '#title' => t('Hour'),
    '#options' => $hours,
    '#default_value' => $interval_values['sub_interval_hour'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'year'),
          array('value' => 'month'),
          array('value' => 'week'),
          array('value' => 'day'),
        ),
      ),
    ),
  );

  // Select a minute if year, month, week or day is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_minute'] = array(
    '#type' => 'select',
    '#title' => t('Minute'),
    '#options' => $minutes,
    '#default_value' => $interval_values['sub_interval_minute'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'year'),
          array('value' => 'month'),
          array('value' => 'week'),
          array('value' => 'day'),
        ),
      ),
    ),
  );

  // Select a weekday if week is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_custom'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom interval in seconds'),
    '#default_value' => $interval_values['sub_interval_custom'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array('value' => 'custom'),
      ),
    ),
    '#size' => 20,
  );

  // Select an hour if year, month, week or day is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_custom_starting_hour'] = array(
    '#type' => 'select',
    '#title' => t('Starting hour'),
    '#options' => $hours,
    '#default_value' => $interval_values['sub_interval_custom_starting_hour'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'custom'),
        ),
      ),
    ),
  );

  // Select a minute if year, month, week or day is chosen as main.
  $form['configuration'][$default_values['id']]['interval_data']['sub_interval_custom_starting_minute'] = array(
    '#type' => 'select',
    '#title' => t('Starting minute'),
    '#options' => $minutes,
    '#default_value' => $interval_values['sub_interval_custom_starting_minute'],
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'custom'),
        ),
      ),
    ),
  );

  // Add some actions if this is not a new interval.
  if ($default_values['id'] != 'new') {
    $form['configuration'][$default_values['id']]['actions'] = array(
      '#type' => 'container',
      '#prefix' => '<div class="actions">',
      '#suffix' => '</div>',
    );

    // Provide the ability to run this interval's code outside of the cron.
    $form['configuration'][$default_values['id']]['actions']['test'] = array(
      '#markup' => l(t('Test run'), current_path() . '/test_run/' . $default_values['id'], array(
        'query' => array(
          // Add a token to make sure this link cannot be called from
          // another place.
          'token' => drupal_get_token(),
        ),
      )),
    );

    // Enable or disable this interval.
    $form['configuration'][$default_values['id']]['actions']['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#default_value' => $default_values['status'],
    );

    // Mark for deletion.
    $form['configuration'][$default_values['id']]['actions']['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
    );
  }

  // Snap the interval minutes to certain parts of an hour.
  $form['configuration'][$default_values['id']]['interval_data']['main_interval_interval_minute_snap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Snap to parts of an hour if possible'),
    '#description' => t('When enabled, an attempt is done to divide an hour by the entered number of minutes.<br />If the result is a rounded number, the next cron run will snap to the closest part of the hour.<p>E.g. every 15 minutes could snap to 0:00, 0:15, 0:30 and 0:45.</p>'),
    '#default_value' => $interval_values['main_interval_interval_minute_snap'],
    '#prefix' => '<div class="interval-minute-snap">',
    '#suffix' => '</div>',
    '#states' => array(
      'visible' => array(
        ':input[name="configuration[' . $default_values['id'] . '][interval_data][main_interval]"]' => array(
          array('value' => 'minute'),
        ),
      ),
    ),
  );
}

/**
 * Validate function for cron_interval_actions_form.
 *
 * Checks if either name, machine name or main interval is set.
 *
 * Adds the changed attribute to the fields so we can check if the value of a
 * field has changed.
 *
 * Adds the recalculate_next_run attribute so the save function knows if the
 * next run timestamp must be recalculated.
 */
function cron_interval_actions_form_validate(&$form, &$form_state) {
  // Get form children.
  $elements = element_children($form['configuration']);

  foreach ($elements as $key) {
    // Before doing anything else, check if a valid custom interval is set in
    // all intervals.
    if (
      isset($form_state['values']['configuration'][$key]['interval_data']['sub_interval_custom']) &&
      isset($form_state['values']['configuration'][$key]['interval_data']['main_interval'])
    ) {
      $main_interval = $form_state['values']['configuration'][$key]['interval_data']['main_interval'];
      $custom_interval = $form_state['values']['configuration'][$key]['interval_data']['sub_interval_custom'];

      if ($main_interval == 'custom' && ((int) $custom_interval != $custom_interval || (int) $custom_interval < 1)) {
        form_set_error('configuration][' . $key . '][interval_data][sub_interval_custom', t('Please enter a valid custom interval. This should be a positive number representing how many seconds must pass until the next cron run.'));
      }
    }

    // Check if a valid integer is entered in the interval number field.
    if (
      isset($form_state['values']['configuration'][$key]['interval_data']['main_interval_interval']) &&
      isset($form_state['values']['configuration'][$key]['interval_data']['main_interval'])
    ) {
      $main_interval = $form_state['values']['configuration'][$key]['interval_data']['main_interval'];
      $main_interval_interval = $form_state['values']['configuration'][$key]['interval_data']['main_interval_interval'];

      if ($main_interval != 'custom' && $main_interval != 'none' && ((int) $main_interval_interval != $main_interval_interval || (int) $main_interval_interval < 1)) {
        form_set_error('configuration][' . $key . '][interval_data][main_interval_interval', t('Please enter a valid interval number. This should be a positive number representing how much time must pass until the next cron run.'));
      }
    }

    // Check if a new interval is being added.
    if ($key == 'new') {
      // Because we need the changed attribute, set it to TRUE.
      if (isset($form_state['values']['configuration'][$key])) {
        $form_state['values']['configuration'][$key]['#changed'] = TRUE;
      }

      // Check if name, machine_name and main_interval are empty and sum
      // the boolean results as an integer.
      $if_sum = empty($form_state['values']['configuration'][$key]['name'])
        + empty($form_state['values']['configuration'][$key]['machine_name'])
        + ($form_state['values']['configuration'][$key]['interval_data']['main_interval'] == 'none');

      // To validate, we need all values (sum 0) or no values (sum 3). If either
      // of the three values is set, we need to check if the other values are
      // set too.
      if ($if_sum > 0 && $if_sum < 3) {
        form_set_error('configuration][new', t('Please enter a name, machine name and interval.'));
      }

      // No need to check for changed values when adding a new interval.
      continue;
    }

    // Continue if there are no children or when marked for deletion.
    if (
      empty($form_state['values']['configuration'][$key]) ||
      !empty($form_state['values']['configuration'][$key]['actions']['delete'])
    ) {
      continue;
    }

    // Loop through all the submitted fields in the form state for this
    // interval.
    foreach ($form_state['values']['configuration'][$key] as $field => $value) {
      // Set the changed en recalculate_next_run attribute by default to FALSE.
      $form_state['values']['configuration'][$key]['#changed'] = FALSE;
      $form_state['values']['configuration'][$key]['#recalculate_next_run'] = FALSE;

      // Check if this field exists in the form.
      if (isset($form['configuration'][$key][$field])) {
        // Check if the value is a field group, only do this for one level!
        if (is_array($value)) {
          // Loop through the subfields.
          foreach ($value as $subfield => $subvalue) {
            // Check if the field has a default value key.
            // If not, it might be a container div so we skip this one.
            if (!isset($form['configuration'][$key][$field][$subfield]['#default_value'])) {
              continue;
            }

            // Store the old and new values in variables.
            $old_value = $form['configuration'][$key][$field][$subfield]['#default_value'];
            $new_value = $subvalue;

            // Compare the default value in the form with the new value in the
            // form state.
            if ($old_value != $new_value) {
              // If they don't match, set changed to TRUE.
              $form_state['values']['configuration'][$key]['#changed'] = TRUE;

              // Perform an extra check if the interval data has changed.
              if ($field == 'interval_data') {
                // In this case, let the save function know the next run
                // timestamp must be recalculated.
                $form_state['values']['configuration'][$key]['#recalculate_next_run'] = TRUE;
              }

              // Now we know at least one value has changed, there is no need
              // to check the other values so break out of both loops.
              break 2;
            }
          }
        }
        // Do the same checks if it's just a normal field.
        else {
          // Check if the field has a default value key.
          // If not, it might be a container div so we skip it.
          if (!isset($form['configuration'][$key][$field]['#default_value'])) {
            continue;
          }

          // Store the old and new values in variables.
          $old_value = $form['configuration'][$key][$field]['#default_value'];
          $new_value = $value;
        }

        // Compare the default value in the form with the new value in the form
        // state.
        if ($old_value != $new_value) {
          // If they don't match, set changed to TRUE.
          $form_state['values']['configuration'][$key]['#changed'] = TRUE;
          // Now we know at least one value has changed, there is no need
          // to check the other values.
          break;
        }
      }
    }
  }
}

/**
 * Submit function for cron_interval_actions_form.
 */
function cron_interval_actions_form_submit($form, &$form_state) {
  // Update status.
  if (empty($form_state['values']['status']['cron_interval_actions_enabled'])) {
    variable_set('cron_interval_actions_enabled', FALSE);
  }
  else {
    variable_set('cron_interval_actions_enabled', TRUE);
  }

  // Check if there is any interval configuration.
  if (!empty($form_state['values']['configuration'])) {
    // Define an empty array for storing interval id's that must be deleted.
    $delete = array();

    // Loop through all intervals.
    foreach ($form_state['values']['configuration'] as $id => $interval) {
      // If the name, machine name or main interval is empty, do nothing.
      if (
        empty($interval['name']) ||
        empty($interval['machine_name']) ||
        empty($interval['interval_data']['main_interval']) ||
        $interval['interval_data']['main_interval'] == 'none'
      ) {
        continue;
      }

      // If the interval is marked for deletion, add it to the delete array.
      if (!empty($interval['actions']['delete'])) {
        $delete[] = $id;
      }
      // Otherwise, check if the values have changed and perform a save.
      elseif ($interval['#changed']) {
        if (cron_interval_actions_save_interval($interval, $id)) {
          // Let the user know the create or update succeeded.
          if (empty($id) || $id == 'new') {
            drupal_set_message(t('A new interval has been added.'));
          }
          else {
            drupal_set_message(t('Interval @name has been updated.', array(
              '@name' => $interval['name'],
            )));
          }
        }
        else {
          // Let the user know the create or update failed.
          if (empty($id) || $id == 'new') {
            drupal_set_message(t('There was an error creating the new interval. Please try again.'), 'error');
          }
          else {
            drupal_set_message(t('There was an error updating interval @name. Please try again.', array(
              '@name' => $interval['name'],
            )), 'error');
          }
        }
      }
    }
  }

  // If there are intervals that must be deleted, redirect the user to the
  // confirm deletion form.
  if (!empty($delete)) {
    $form_state['redirect'] = 'admin/config/system/cron_interval_actions/delete/' . implode('-', $delete);
  }
}

/**
 * Check if a machine name exists in the table cron_interval_actions.
 *
 * @param string $value
 *   String containing a machine name.
 *
 * @return bool
 *   A boolean indicating if the given machine name exists or not.
 */
function cron_interval_actions_machine_name_exists($value) {
  $result = db_select('cron_interval_actions', 'cia')
    ->fields('cia', array('id'))
    ->condition('machine_name', $value)
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();

  if (!empty($result)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Generates a confirm deletion form.
 *
 * The $ids parameter is a string containing the interval id's to be deleted.
 * Multiple id's are separated by '-'.
 */
function cron_interval_actions_delete_form($form, &$form_state, $ids = '') {
  // Redirect the user back to the interval overview if there are no id's.
  if (empty($ids)) {
    drupal_goto('admin/config/system/cron_interval_actions');
  }

  // Set a title.
  drupal_set_title(t('Are you sure you want to delete the following interval(s)?'));

  // Put the id's in an array.
  $ids = explode('-', $ids);
  // Load intervals with the obtained id's.
  $options = array(
    'id' => $ids,
  );
  $intervals = cron_interval_actions_load_intervals($options);

  // Define an empty list items array.
  $list_items = array();
  // Loop through the intervals and put their names in the list items array.
  foreach ($intervals as $interval) {
    $list_items[] = check_plain($interval['name']);
  }

  // Define an empty form.
  $form = array();

  // Attach custom css.
  $form['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'cron_interval_actions') . '/css/cron_interval_actions.css',
    ),
  );

  // Add a class to our form.
  $form['#attributes'] = array(
    'class' => array(
      'cia_delete_form',
    ),
  );

  // Pass the interval id's to the submit function.
  $form['interval_ids'] = array(
    '#type' => 'value',
    '#value' => $ids,
  );

  // Just to let you know...
  $form['message'] = array(
    '#markup' => t('This action cannot be undone.'),
  );

  // Display a list of interval names the user is about to delete.
  $form['intervals'] = array(
    '#theme' => 'item_list',
    '#title' => '',
    '#items' => $list_items,
  );

  // Put a div around the buttons to force them below the form.
  $form['actions'] = array(
    '#type' => 'container',
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  // Just a submit button.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  // The cancel button takes the user back to the intervals overview.
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/system/cron_interval_actions'),
  );

  return $form;
}

/**
 * Submit function for cron_interval_actions_delete_form.
 */
function cron_interval_actions_delete_form_submit($form, &$form_state) {
  // Check if we have any interval id's.
  if (!empty($form_state['values']['interval_ids'])) {
    // Perform the deletion.
    cron_interval_actions_delete_intervals($form_state['values']['interval_ids']);
  }

  // Redirect the user back to the intervals overview.
  $form_state['redirect'] = 'admin/config/system/cron_interval_actions';
}
