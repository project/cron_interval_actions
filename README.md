README.md
==========

Cron Interval Actions is a module to create hooks which will be executed on
timed intervals ran by the cron.

When this module is enabled, an administration page becomes available on
admin/config/system/cron_interval_actions.
This administration page brings the permission 'Administer Cron Interval Action
Settings'.

On this page it is possible to create multiple intervals. Each interval
generates an unique hook generated from the interval name. Once created, the
interval name can be changed but the hook name stays the same.

This hook can be implemented in your own module. All code in this hook will be
executed every interval you have configured.

For example: You create an interval named 'Monthly Digest' and you set the
interval to the first day of every month.
The hook 'hook_cia_action_monthly_digest' becomes
available. Every first of the month this hook gets called by the cron and
executes your code. See below for an example hook.

Cron Interval Actions are exportable by features.

IMPORTANT
---------
The actual cron job must match the lowest interval you create to work
properly. If you configure your cron to run every 10 minutes, all intervals
below 10 minutes will not work properly.

WARNING
-------
To test your code in your interval hook, a 'Test run' button is available for
every interval.

When executing a test run, all code in your hook will be executed.
So if you're sending a digest or notices per e-mail, the e-mails WILL be sent.
To prevent this, a parameter $test will be provided in your hook allowing you
to take precautions.

EXAMPLE HOOK
------------
    function MY_MODULE_cia_action_montly_digest($test = FALSE) {
      // Here comes your code.
      // If this hook is called via the test run, $test will be TRUE.
    }
