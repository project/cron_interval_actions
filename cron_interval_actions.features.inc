<?php

/**
 * @file
 * Holds features implementation.
 */

/**
 * Implements hook_features_api().
 */
function cron_interval_actions_features_api() {
  return array(
    'cron_interval_actions' => array(
      'name' => 'Cron Interval Actions',
      'file' => drupal_get_path('module', 'cron_interval_actions') . '/cron_interval_actions.features.inc',
      'default_hook' => 'cron_interval_actions_info',
      'feature_source' => TRUE,
    ),
  );
}

/**
 * Helper function to return the interval names to show in the feature UI.
 *
 * @return array
 *   A keyed array of items, suitable for use with a FormAPI select or
 *   checkboxes element.
 */
function cron_interval_actions_features_export_options() {
  $return = array();

  $intervals = cron_interval_actions_load_intervals();

  foreach ($intervals as $interval) {
    $return[$interval['machine_name']] = $interval['name'];
  }

  return $return;
}

/**
 * Implements hook_features_export().
 */
function cron_interval_actions_features_export($data, &$export, $module_name = '') {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['cron_interval_actions'] = 'cron_interval_actions';

  $intervals = cron_interval_actions_load_intervals();
  foreach ($intervals as $interval) {
    if (in_array($interval['machine_name'], $data)) {
      $export['features']['cron_interval_actions'][$interval['machine_name']] = $interval['machine_name'];
    }
  }
  return $export;
}

/**
 * Implements hook_features_export_render().
 */
function cron_interval_actions_features_export_render($module, $data, $export = NULL) {
  $intervals = cron_interval_actions_load_intervals();
  $code = array();
  foreach ($data as $machine_name) {
    foreach ($intervals as $interval) {
      if ($interval['machine_name'] == $machine_name) {
        // Unset some vars because they are environment dependent.
        unset($interval['id']);
        unset($interval['last_run']);
        unset($interval['next_run']);
        $code[] = $interval;
      }
    }
  }
  $code = "  return " . features_var_export($code, '  ') . ";";
  return array('cron_interval_actions_info' => $code);
}

/**
 * Implements hook_features_revert().
 */
function cron_interval_actions_features_revert($module) {
  cron_interval_actions_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 *
 * Rebuilds cron interval actions from code defaults.
 */
function cron_interval_actions_features_rebuild($module) {
  $saved_intervals = module_invoke($module, 'cron_interval_actions_info');

  foreach ($saved_intervals as $interval) {
    // Unserialize first because drupal write serializes it again.
    $interval['interval_data'] = unserialize($interval['interval_data']);
    // Calculate the next run.
    $interval['next_run'] = cron_interval_actions_calculate_next_run($interval['interval_data']);

    // Check if we can find an existing cia.
    $result = db_select('cron_interval_actions', 'cia')
      ->fields('cia')
      ->condition('cia.machine_name', $interval['machine_name'])
      ->range(0, 1)
      ->execute()
      ->fetchAssoc();

    // If not, add a new one.
    if (empty($result)) {
      drupal_write_record('cron_interval_actions', $interval);
    }
    // Otherwise, update the existing one.
    else {
      $interval['id'] = $result['id'];
      drupal_write_record('cron_interval_actions', $interval, 'id');
    }
  }
}
